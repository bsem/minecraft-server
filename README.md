# Minecraft Server

## Steps

1. Create GCP Project and enable Compute Engine API
2. Create GCP Service Account (e.g. `terraform`) with Compute Engine Admin role. Create and download GCP SA private key.
3. Configure gcloud CL

```
gcloud init
```

4. Set environment variables for Terraform auth with GCP.

```
export GOOGLE_CLOUD_KEYFILE_JSON=<path to downloaded GPC SA private key>
export GOOGLE_CLOUD_PROJECT=<GCP Project ID>
```

4. Setup Terraform and deploy resources.

```
cd terraform
terraform init
terraform plan
terraform apply -auto-approve
# terraform apply -destroy
```
