resource "google_compute_instance" "minecraft-server" {
  name         = "minecraft-server"
  machine_type = "e2-medium"
  zone         = "europe-central2-a"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }

  lifecycle {
    ignore_changes = [attached_disk]
  }

  tags = ["http", "https", "ssh"]
  //metadata_startup_script = "echo hi > /test.txt" < Docker installation script here probably and mounting attached disk
}

resource "google_compute_attached_disk" "minecraft_server_attached_disk" {
  disk     = google_compute_disk.minecraft_server_disk.id
  instance = google_compute_instance.minecraft-server.id
}

resource "google_compute_disk" "minecraft_server_disk" {
  name  = "minecraft-server-disk"
  type  = "pd-standard"
  image = "ubuntu-os-cloud/ubuntu-2204-lts"
  zone  = "europe-central2-a"
  size  = 30
}

resource "google_compute_firewall" "allow-http-https-ssh" {
  name    = "allow-http-https-ssh"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http", "https", "ssh"]
}